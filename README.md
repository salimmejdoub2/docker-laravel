# CLONE PROJECT && BUILD DOCKER CONTAINERS

```shell

git clone https://salimmejdoub2@bitbucket.org/salimmejdoub2/docker-laravel.git sm-docker-larvel
cd dsm-docker-larvel/
docker-compose up -d

```

## Configuration

This package comes with default configuration options. You can modify them by creating `.env` file in your root directory.*


## phpMyAdmin

phpMyAdmin is configured to run on port 8080 (port number can be changed in .env in root file)  . Use following default credentials.

http://localhost:8080/   
username: root  
password: tiger

