import Axios from "axios";

export const getTaskList = () => {};

/**
 * storeNewTask()
 *
 * @param {object} data
 */
export const storeNewTask = async (data) => {
  data.project_id = parseInt(data.project_id);

  return await Axios.post("/api/tasks", data).then(
    (res) => {
      return res.data;
    }
  );
};

/**
 * Update Task
 * @param id
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */
export const updateTask = async (id, data) => {
  return await Axios.put(
    `/api/tasks/${id}`,
    data
  ).then((res) => {
    return res.data;
  });
};

/**
 * Delete Tasks
 * @param id
 * @returns {Promise<AxiosResponse<T>>}
 */
export const deleteTask = async (id) => {
  return await Axios.delete(
    `/api/tasks/${id}`
  ).then((res) => {
    return res.data;
  });
};
