<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'name' => 'John Doe',
            'email' => 'user@todo.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
