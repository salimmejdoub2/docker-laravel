<?php

use App\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Task::create([
            'name' => 'Tâche 1',
            'description' => 'Établir une liste des personnes à inviter',
            'status' => 1,
            'project_id' => 1
        ]);

        Task::create([
            'name' => 'Tâche 2',
            'description' => 'Gérer les invitations : rédaction, présentation et envoi',
            'project_id' => 1
        ]);

        Task::create([
            'name' => 'Tâche 3',
            'description' => 'Envoyer une lettre d’appel d’offres aux traiteurs',
            'project_id' => 1
        ]);

        Task::create([
            'name' => 'Tâche 4',
            'description' => 'Louer du mobilier (tables/chaises)',
            'project_id' => 1
        ]);

        Task::create([
            'name' => 'Tâche 5',
            'description' => 'Dépouiller les coupons-réponses',
            'project_id' => 1
        ]);
    }
}
